
import urllib.request# url request
import re            # regular expression
import os            # dirs
import time
'''
url: website
pattern: key words of regular expression
Directory: the dictionary to put data
'''

    
def BatchDownload(url,pattern,Directory):

    # pull the request
    headers = {'User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'}
    opener = urllib.request.build_opener()
    opener.addheaders = [headers]
    
    # scrawl the data from the website
    content = opener.open(url).read().decode('utf8')
    
    # using keywors download data 
    raw_hrefs = re.findall(pattern, content, 0)
    
    # remove the duplicate part
    hset = set(raw_hrefs)
         
    # download the linked data
    for href in hset:
        if(len(hset)>1):
            link = url + href[0]
            filename = os.path.join(Directory, href[0])
            print("Downloading...",filename)
            urllib.request.urlretrieve(link, filename)
            print("Successful!")
        else:
            link = url +href
            filename = os.path.join(Directory, href)
            print("Downloading...",filename)
            urllib.request.urlretrieve(link, filename)
            print("Successful!")
            
        # sleep interval
        time.sleep(1)


#BatchDownload('https://www1.ncdc.noaa.gov/pub/data/swdi/stormevents/csvfiles/',
#              '(StormEvents_details-ftp_v1.0_d(*\d)_c(*\d).csv.gz)',
#              'C:\Anaconda3\POP_UP\Data')

#BatchDownload('ftp://ftp.chg.ucsb.edu/pub/org/chg/products/CHIRPS-2.0/global_daily/netcdf/p05/',
#              '(chirps-v2.0.(\d*).days_p05.nc)',
#             'C:\Anaconda3\POP_UP\Data')

#BatchDownload('https://www.esrl.noaa.gov/psd/cgi-bin/db_search/DBListFiles.pl?did=118&tid=40290&vid=2227/',
#              '(air.2m.(\d*).nc)',
#             'C:\Anaconda3\POP_UP\Data')


