# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 12:34:27 2019

@author: juli
"""

'''libraries for data download'''
import urllib.request# url request
import re            # regular expression
#import os            # dirs
import time

'''libraries for data process'''
import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

'''functions for data download'''
def BatchDownload(url,pattern,Directory):
    # pull the request
    headers = {'User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'}
    opener = urllib.request.build_opener()
    opener.addheaders = [headers]
    # scrawl the data from the website
    content = opener.open(url).read().decode('utf8')
    # using keywors download data 
    raw_hrefs = re.findall(pattern, content, 0)
    # remove the duplicate part
    hset = set(raw_hrefs)    
    # download the linked data
    for href in hset:
        if(len(hset)>1):
            link = url + href[0]
            filename = os.path.join(Directory, href[0])
            print("Downloading...",filename)
            urllib.request.urlretrieve(link, filename)
            print("Successful!")
        else:
            link = url +href
            filename = os.path.join(Directory, href)
            print("Downloading...",filename)
            urllib.request.urlretrieve(link, filename)
            print("Successful!")     
        # sleep interval
        time.sleep(1)

#path_of_nc_file = "C:\Anaconda3\POP_UP\Data\EC-Interim_monthly_2018.nc"

      
'''functions to show the information of nc file'''
def ShowInfonc():
#show the information of the nc file
    data = nc.Dataset("C:\Anaconda3\POP_UP\Data\EC-Interim_monthly_2018.nc")
    for variables in data.variables.keys():
        print(data.variables[variables])

'''give the file you want to show, and tell the exact month'''
def read_ncfiles_mean_temp(path, month_num):
    data = nc.Dataset(path)
    lat = data.variables['latitude'][:].squeeze()
    lon = data.variables['longitude'][:].squeeze()
    #time = data.variables['time'][:].squeeze()
    airtemp = data.variables['t2m'][:].squeeze()
    airtemp = airtemp[month_num, :, :]
    return lat, lon, airtemp

def plot_basemap_color(lat, lon, data_in):
    plt.figure(num=1, figsize=(8, 8))
    m = Basemap(projection='cyl')
    m.drawcoastlines()
    #m.drawstates()
    #m.drawlsmask()
    lon, lat = np.meshgrid(lon, lat)
    x, y = m(lon, lat)
    cs = m.contourf(x, y, data_in, 25, cmap=plt.cm.jet)
    m.colorbar(cs)
    m.drawparallels(np.arange(-90, 90, 10), labels=[1, 0, 0, 0])
    m.drawmeridians(np.arange(-180, 180, 10), labels=[0, 0, 0, 1])
    plt.show()

def plot_basemap_contour(lat, lon, data_in):
    plt.figure(num=2, figsize=(8, 8))
    m = Basemap(projection='cyl')
    m.drawcoastlines()
    #m.drawstates()
    #m.drawlsmask()
    lon, lat = np.meshgrid(lon, lat)
    x, y = m(lon, lat)
    m.drawparallels(np.arange(-90, 90, 10), labels=[1, 0, 0, 0])
    m.drawmeridians(np.arange(-180, 180, 10), labels=[0, 0, 0, 1])
    curve = m.contour(x, y, data_in, 25, colors="k")
    plt.clabel(curve, fmt='%1.0f')
    plt.show()


#BatchDownload('https://www1.ncdc.noaa.gov/pub/data/swdi/stormevents/csvfiles/',
#              '(StormEvents_details-ftp_v1.0_d(*\d)_c(*\d).csv.gz)',
#              'C:\Anaconda3\POP_UP\Data')
ShowInfonc()
x, y, z = read_ncfiles_mean_temp("C:\Anaconda3\POP_UP\Data\EC-Interim_monthly_2018.nc", 6)
plot_basemap_color(x, y, z)
#plot_basemap_contour(x, y, z)
