'''libraries for data download'''
import urllib.request # url request
import re             # regular expression
import os             # dirs
import sys             # dirs

os.environ['PROJ_LIB'] = r'C:\Anaconda3\pkgs\proj4-5.1.0-hfa6e2cd_1\Library\share'

'''libraries for data process'''
import netCDF4 as nc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap



'''functions for data download'''
def BatchDownload(url,pattern,Directory):

    opener = urllib.request.build_opener()
    

    # scrawl the data from the website
    content = opener.open(url).read().decode('utf8')

    # using keywors download data 
    raw_hrefs = re.findall(pattern, content, 0)
    
    # remove the duplicate part
    hset = set(raw_hrefs)
    
    # download the linked data
    for href in hset:
        if(len(hset)>1):
            link = url + href[0]
            filename = os.path.join(Directory, href[0])
            print("Downloading...",filename)
            urllib.request.urlretrieve(link, filename)
            print("Successful!")
        else:
            link = url +href
            filename = os.path.join(Directory, href)
            print("Downloading...",filename)
            urllib.request.urlretrieve(link, filename)
            print("Successful!")     



'''functions to show the information of nc file'''
def ShowInfonc(path_of_nc):

    #show the information of the nc file
    data = nc.Dataset(path_of_nc)
    for variables in data.variables.keys():
        print(data.variables[variables])




'''give the file you want to show, and tell the exact month'''
def read_ncfiles_mean_temp(path_of_nc, month_num):

    data = nc.Dataset(path_of_nc)

    lat = data.variables['latitude'][:].squeeze()
    lon = data.variables['longitude'][:].squeeze()
    #time = data.variables['time'][:].squeeze()

    airtemp = data.variables['t2m'][:].squeeze()
    airtemp = airtemp[month_num, :, :]

    return lat, lon, airtemp



'''plot the world temperature distribution'''
def plot_distribution_temp(lat, lon, data_in):

    #set the size of figure
    #plt.figure(figsize=(8, 8))
    
    #select the projection and resolution
    m = Basemap(projection = 'merc',llcrnrlat = -80,urcrnrlat = 80,
                llcrnrlon = -180, urcrnrlon = 180,lat_ts = 20, resolution = 'c')

    #the background of the map
    m.drawcoastlines()
    m.drawlsmask()
    lon, lat = np.meshgrid(lon, lat)

    #set some properties of the figure
    x, y = m(lon, lat)
    cs = m.contourf(x, y, data_in, 25, cmap=plt.cm.jet)

    #set some properties of the figure
    m.drawparallels(np.arange(-80, 81, 20), labels=[20, 0, 0, 0], fontsize = 5)
    m.drawmeridians(np.arange(-180, 181, 30), labels=[0, 0, 0, 30], fontsize = 5)
    cbar = m.colorbar(cs, location = 'bottom', pad = '5%', label = 'temperature(k)')
    plt.show()



'''draw the world temperature contour'''
def plot_contour_temperature(lat, lon, data_in):

    #set the size of figure
    #plt.figure(num=2, figsize=(8, 8))

    #select the projection and resolution
    m = Basemap(projection = 'merc',llcrnrlat = -80,urcrnrlat = 80,
                llcrnrlon = -180, urcrnrlon = 180,lat_ts = 20, resolution = 'c')

    #draw the background 
    m.drawmapboundary(fill_color = 'aqua')
    m.fillcontinents(color = 'coral', lake_color = 'aqua')
    #m.drawstates()
    #m.drawlsmask()
    lon, lat = np.meshgrid(lon, lat)
    x, y = m(lon, lat)

    #main part of the map
    m.drawparallels(np.arange(-80, 81, 20), labels = [20, 0, 0, 0], fontsize = 5)
    m.drawmeridians(np.arange(-180, 181, 30), labels = [0, 0, 0, 30], fontsize = 5)
    data_convertion = data_in - 273.15
    curve = m.contour(x, y, data_convertion, 25, colors = 'k')

    #set some properties of the figure
    plt.clabel(curve, fmt = '%1.0f')
    plt.show()



'''extract point data into csv'''
def ReadNctoCsv(lon, lat):
    airtemp = data.variables['t2m'][:]
    airtemp = airtemp[month_num, lon, lat]
    dtime = data.variables['time'][:]



'''merge multiple nc data in a file'''
def Multiple_nc_file_merge(file_name_path):
    
    #two ways for reading nc file
    #da = nc.Dataset(file_name_path,'w',format='netCDF4')
    ds = xr.open_dataset(file_name_path)
    lonS = ds['lon']
    latS = ds['lat']
    timeS = ds['time']

    #establish the coordinate
    da.createDimension('lon',144)  
    da.createDimension('lat',73)
    da.createDimension('time',12)
    da.createVariable("lon",'f',("lon"))  
    da.createVariable("lat",'f',("lat"))
    da.createVariable("time",'f',("time"))

    #fill data
    da.variables['lat'][:] = latS
    da.variables['lon'][:] = lonS
    da.variables['time'][:] = timeS

    #establish the merged variable
    da.createVariable('tm','f8',('lat','lon','time'))
    da.variables['tm'][:] = data
    da.close()




'''trend of climate change'''
#def trends_of_annual(data,file_name_path):


#def trends_of_extrem(data,file_name_path):


'''Application of the functions'''
#BatchDownload('https://www1.ncdc.noaa.gov/pub/data/swdi/stormevents/csvfiles/',
#              '(StormEvents_details-ftp_v1.0_d(*\d)_c(*\d).csv.gz)',
#              'C:\Anaconda3\POP_UP\Data')

#BatchDownload('ftp://ftp.chg.ucsb.edu/pub/org/chg/products/CHIRPS-2.0/global_daily/netcdf/p05/',
#              '(chirps-v2.0.(\d*).days_p05.nc)',
#             'C:\Anaconda3\POP_UP\Data')

#BatchDownload('https://www.esrl.noaa.gov/psd/cgi-bin/db_search/DBListFiles.pl?did=118&tid=40290&vid=2227/',
#              '(air.2m.(\d*).nc)',
#             'C:\Anaconda3\POP_UP\Data')



#path_of_nc = 'c:/Anaconda3/POP_UP/Data/xEC-Interim_monthly_2018.nc'

#ShowInfonc(path_of_nc)

#x, y, z = read_ncfiles_mean_temp(path_of_nc, 6)
#plot_distribution_temp(x, y, z)
#plot_contour_temperature(x, y, z)

